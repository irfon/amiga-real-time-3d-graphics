*****************************************************************************************
*                                 Core_08.s                                             *
*                  Subroutines for euler_scn Chapter 12                                 *
*****************************************************************************************
 include core_07.s    previous subroutines

joy_look:
* Change the euler angles etheta and ephi (vtheta and vphi from chapter 10 are the same)
* Read the joystick and update the variables accordingly

  move.w  $dff00c,d0    joystick 2
  btst    #8,d0
  beq     eright,dn
  btst    #9,d0
  beq     eup_jy
  bra     eleft_jy

eright_dn:
  btst    #0,d0
  beq     eout_jy
  btst    #1,d0
  beq     edown_jy
  bra     eright_jy
eout_jy rts

eup_jy:
  bsr     erot_down     rotate view frame down about vy axis
  rts
edown_jy:
  bsr     erot_up       rotate up about vy axis
  rts
eleft_jy:
  bsr     erot_left     rotate left aobut vx axis
  rts
eright_jy:
  bsr     erot_right    rotate right about wx axis
  rts

erot_down:
* Rotate down about the yv axis. Decrement ephi (same as vphi)
  move.w  #-5,vph_inc
  rts

erot_up:
* Rotate up about the xw axis. Increment ephi (same as vphi)
  move.w  #5,vphi_inc
  rts
erot_left:
* Rotate left about the xw axis. Increment etheta
  move.w  #5,vtheta_inc
  rts 
erot_right:
* Rotate right about the xw axis. Decrement etheta
  move.w  #-5,vtheta_inc
  rts 
vtran_move:
* move the view transform matrix to the base vectors
* really just a change of label
 lea iv,a0
 lea jv,a1
 lea kv,a2
 lea w_vmatx,a3
 move.w (a3)+,(a0)+ all
 move.w (a3)+,(a0)+ iv
 move.w (a3)+,(a0)+
 move.w (a3)+,(a1)+ all
 move.w (a3)+,(a1)+ jv
 move.w (a3)+,(a1)+
 move.w (a3)+,(a2)+ all
 move.w (a3)+,(a2)+ kv
 move.w (a3),(a2)
 rts
 

