***************************************************************************************
* Polygon demo - not using OS.
*
* opens a view, loads a raw gfx file for display background
* rotates a filled polygon, quits on LMB. - NOT YET!
*
***************************************************************************************
*	Call this file from PFdemo.header when assembling. 
***************************************************************************************

USE_FILES EQU 1
USE_DISPLAY EQU 1
EXTENDED EQU 1

USE_VBLANK EQU 1
;TESTING_VBLANK EQU 1		switch to turn only writing gfx to one bitmap

	opt warnbit

;	incdir	include:
;	include	Graphics/Gfx.i
;	include	Graphics/GfxBase.i
;	include	Graphics/view.i
;	include	Graphics/videocontrol.i
;	include Graphics/rastport.i
;	include	Graphics/graphics_lib.i
;	include Libraries/lowlevel_lib.i
;	include	Intuition/intuition_lib.i
;	include	Exec/exec_lib.i	
;	include	Dos/dos_lib.i
	
;	include	exec/memory.i
	
;	include	dos/dosextens.i
	
;	include	hardware/custom.i
		
;	include	FilledPolygon.equates
		
***************************************************************************************
Start
	movem.l   d0/a0,-(a7)								 Save argc and argv.
 suba.l    a1,a1               Clr task name to NULL so we can...
 movea.l   4.w,a6
 jsr       _LVOFindTask(a6)    Find this task and save the
 movea.l   d0,a4               pointer to the task/process control block.
 tst.l     pr_CLI(a4)          See if we came from the CLI
 bne.s     CLI_Launch          Yes; so carry on.
 lea       pr_MsgPort(a4),a0   Else WBench launched; Handle the startup
 movea.l   4.w,a6														message WB sends...
 jsr       _LVOWaitPort(a6)				Wait for the message to arrive...
 lea       pr_MsgPort(a4),a0
 jsr       _LVOGetMsg(a6)						..and get it. Then save it to
 move.l    d0,ReturnMsg								reply when our process is done.

CLI_Launch
 movem.l   (a7)+,d0/a0									Get argc and argv back.(Don't need 'em tho).
 
 jsr							openlibraries
 tst.l					d0
 bmi 						Close_Libs
	
	jsr							allocmem													ask for memory and other structures
	tst.l					d0
	bmi							FreeAll
	
	IFD USE_FILES	
*load any files
	move.l				File_Buffer,d0        												        
 move.l				#File,d1													filename
 bsr    			GetFile		
 tst.l  			d0
 bmi  					FreeAll														couldn`t get file.

	jsr	movegfx																				move loaded data into bitplane memory
	ENDC
	
	jsr	initialise_display									get system type structures initialised	

* now load two pointers that will be used to switch memory for dbuffering (video mem).
	move.l	work_bitmap,a1										Sys alloc'd BitMap structure
	move.l	#bm_Planes,d0											pick up offset to the Planes table
	move.l	(a1,d0),workplanes						load workplane pointer(first pointer,I hope that mem is contiguous for all planes!)				
	move.l	show_bitmap,a1
	move.l	#bm_Planes,d0											pick up offset to the Planes table
	move.l	(a1,d0),showplanes						load showplane pointer ditto as above for video mem 2.				

	IFD USE_VBLANK
*add vblank interrupt routine
 lea	vblank_code,a0													install V_40 vblank interrupt
	move.l	LowLevelBase,a6
	jsr	_LVOAddVBlankInt(a6)
	move.l	d0,intHandle
	beq	 FreeAll 
	ENDC
	
	lea	my_view,a1
 movea.l GrafBase,a6
 jsr  _LVOLoadView(a6)
 jsr  _LVOWaitTOF(a6)												wait for vblank
 jsr  _LVOWaitTOF(a6)

*--------------------------    
; The game is now executed   
	jsr  display
*--------------------------	 
	IFD USE_VBLANK
	move.l	intHandle,d1
	beq	Restore_Display
	move.l	d1,a1
	move.l	LowLevelBase,a6
	jsr	_LVORemVBlankInt(a6)
	ENDC

Restore_Display

; Do a low level restoration of the display.
 movea.l OldActiView,a1										get original view structure
 movea.l GrafBase,a6
 jsr     _LVOLoadView(a6)								& install it.
 jsr     _LVOWaitTOF(a6)									wait for vblank
 jsr     _LVOWaitTOF(a6)
 move.l  gb_copinit(a6),cop1lc	set copperlist ptr to original list.

	jsr	FreeCop_Lists															free colormap and copperlists mem

; why use d1 -> a1. Because a1 wont set the flags and I want to set the Z if
; there's no address in the library or memory pointer.	
		

FreeAll	
 move.l  draw_buffer,d1										return buffer memory to the system
 beq	Free_BufferMem
 move.l	d1,a1
 move.l   #width*height,d0
 movea.l  4.w,a6
 jsr _LVOFreeMem(a6)
	
Free_BufferMem	
 move.l  File_Buffer,d1										return buffer memory to the system
 beq	Free_Bitmaps
 move.l	d1,a1
 move.l   #Filesize,d0
 movea.l  4.w,a6
 jsr _LVOFreeMem(a6)

Free_Bitmaps
	movea.l	GrafBase,a6
	jsr					_LVOWaitBlit(a6)								just in case somethings blitting
	move.l		show_bitmap,d0										the memory we want to free??
	beq	NextBitMap
	move.l	d0,a0
	movea.l	GrafBase,a6
	jsr	_LVOFreeBitMap(a6)										free bitmap memory
NextBitMap	
	move.l		work_bitmap,d0										the memory we want to free??
	beq	Close_Libs
	move.l	d0,a0
	movea.l	GrafBase,a6
	jsr	_LVOFreeBitMap(a6)										free bitmap memory
	
Close_Libs
; Close down any Libraries we managed to open. Leave the ones we didn't!!	
	move.l  LowLevelBase,d1
	move.l	d1,a1
 beq.s    Close_Graf														was LowLevel open?
 movea.l  4.w,a6
 jsr      _LVOCloseLibrary(a6)
Close_Graf															    
	move.l  GrafBase,d1
	move.l	d1,a1
 beq.s    Close_DOS														was Grafix open?
 movea.l  4.w,a6
 jsr      _LVOCloseLibrary(a6)
Close_DOS
 move.l  DosBase,d1
	move.l	d1,a1
 beq.s    Close_Intuition								was DOS open?
 movea.l  4.w,a6
 jsr      _LVOCloseLibrary(a6)
Close_Intuition
 move.l  IntuiBase,d1
	move.l	d1,a1
 beq.s    Quit_Game														was Intuition open?
 movea.l  4.w,a6
 jsr      _LVOCloseLibrary(a6)
Quit_Game
 moveq    #0,d0                  return code
 move.l   d0,-(a7)
 tst.l    ReturnMsg             	were we WBench launched ?
 beq.s    Quit_All               no, so just quit
 movea.l  4.w,a6                
 jsr      _LVOForbid(a6)									yes, so return the message we got.
 movea.l  ReturnMsg(pc),a1
 jsr      _LVOReplyMsg(a6)
Quit_All
 move.l   (a7)+,d0               pick up return code. 0 = success.
 rts																													finished!
********************************************************************************************
display
	bsr	objadd
	movem.l	d0-d7/a0-a6,-(sp)
	IFD KILLTASKS
	movea.l	$4.w,a6
		jsr	_LVOForbid(a6)		
	ENDC
.loop
	
	IFND USE_VBLANK
	jsr	vblank_code
	ENDC

	bsr	filledvectors
				
	btst			#6,$bfe001																														are we fed up yet?
	bne 			.loop																																				no, not yet....
	IFD KILLTASKS			
	movea.l	$4.w,a6
		jsr	_LVOPermit(a6)
	ENDC
	movem.l (sp)+,d0-d7/a0-a6	
	rts
*******************************************************************************************		
openlibraries
 movea.l   4.w,a6
 lea       IntuiName(pc),a1					Open Intuition Library
 moveq     #Version_39,d0     		version 39 or more.
 jsr       _LVOOpenLibrary(a6)
 move.l    d0,IntuiBase        	save result to check on later.
 
 movea.l   4.w,a6
	lea     		LowLevelName(pc),a1 	open lowlevel library
 moveq   		#0,d0
 jsr     		_LVOOpenLibrary(a6)
 tst.l   		d0
 beq   				.errorend												couldn't get lowlevel
	move.l				d0,LowLevelBase
 
 movea.l   4.w,a6
 lea       GrafName(pc),a1						Open Gfx Library
 moveq     #Version_36,d0      	version 33 or more.
 jsr       _LVOOpenLibrary(a6)
 tst.l     d0
 beq      	.errorend          		couldn't get Gfx Library.
 move.l    d0,GrafBase
 movea.l   d0,a6
 move.l    gb_ActiView(a6),OldActiView    									    
 
 lea       DosName,a1											Open DOS Library.
 moveq     #0,d0
 movea.l   4.w,a6
 jsr       _LVOOpenLibrary(a6)
 move.l    d0,DosBase
 beq       .errorend												couldn`t get DOS.
 rts	
.errorend
	move.l	#$FFFFFFFF,d0
	rts
;----------------------------------------------------------------------------------------
initialise_display
				
*initialise this view		
	lea					my_view,a1
 movea.l	GrafBase,a6
 jsr					_LVOInitView(a6)

*initialise the rasinfo structure
.rasinfo	    
 lea				  my_rasinfo,a6
 move.l			#0,ri_Next(a6)
 move.l			work_bitmap,ri_BitMap(a6)						link in the bitmap structure
 move.w			#0,ri_RxOffset(a6)
 move.w			#0,ri_RyOffset(a6)
 
*initialise the rastport structure I need (because I'm using OS to draw!)
	lea						rastport,a1
 movea.l		GrafBase,a6
 jsr						_LVOInitRastPort(a6)
 
*and link bitmap to it
	lea 					rastport,a0
	move.l			work_bitmap,rp_BitMap(a0)
	
*make extra structures that draw routines require!!
	lea						areainfo,a0
	lea						areabuffer,a1										buffer the sys will use to build our objects
	move.l			#NUM_VERTICES*5,d0					size we want buffer to be (*5)
 movea.l		GrafBase,a6
 jsr						_LVOInitArea(a6)
 
*link this structure to the rastport
	lea						rastport,a0
	lea						areainfo,a1
	move.l			a1,rp_AreaInfo(a0)
	
*setup a tmpras structure, it's no wonder that people can't be bothered with the OS!!
	lea						tmpras,a0
	movea.l		draw_buffer,a1
	move.l			#width*height,d0
 movea.l		GrafBase,a6
 jsr						_LVOInitTmpRas(a6)
 
*link the tmpras to the rastport. Christ what a convoluted performance.
	lea						rastport,a0
	lea						tmpras,a1
	move.l			a1,rp_TmpRas(a0)

*initialise the viewport structure    
 lea						my_viewport,a0
 movea.l		GrafBase,a6
 jsr						_LVOInitVPort(a6)

*link the viewport to the view     
 lea						my_view,a6
 lea						my_viewport,a0
 move.l			a0,v_ViewPort(a6)

*initialise the viewport fields
	move.w 		#dxoffset,vp_DxOffset(a0)
	move.w 		#dyoffset,vp_DyOffset(a0)
	move.w 		#vp_width,vp_DWidth(a0)
	move.w 		#vp_height,vp_DHeight(a0)
	lea						my_rasinfo,a1
	move.l			a1,vp_RasInfo(a0)
	
*get the color map structure
.colormap
	move.l			#depth<<2,d0						num of colors
	movea.l		GrafBase,a6
	jsr						_LVOGetColorMap(a6)
	move.l			d0,colormap
	beq						.error
	
*link colormap to viewport	ONLY for 1.3 or less
 lea						my_viewport,a0
	move.l			d0,vp_ColorMap(a0)
	
*load colors into colormap
.loadcolors
	lea						color_table,a1
	move.w 		#depth<<2,d0
	movea.l		GrafBase,a6
	jsr						_LVOLoadRGB4(a6)
	
*build the copperlists - we need two for double buffering
	lea						my_view,a0
	lea						my_viewport,a1
	movea.l		GrafBase,a6
	jsr						_LVOMakeVPort(a6)
	lea						my_view,a1
	movea.l		GrafBase,a6
	jsr						_LVOMrgCop(a6)

*store the first one, and make the next.
	lea						my_view,a0
	move.l			v_LOFCprList(a0),worklist
	move.l			#0,v_LOFCprList(a0)											reset the previous pointer to NULL
	    
 lea				  my_rasinfo,a6
 move.l			show_bitmap,ri_BitMap(a6)					link in the next bitmap structure

	lea						my_view,a0
	lea						my_viewport,a1
	movea.l		GrafBase,a6
	jsr						_LVOMakeVPort(a6)	
	lea						my_view,a1
	movea.l		GrafBase,a6
	jsr						_LVOMrgCop(a6)
	
	lea						my_view,a0
	move.l			v_LOFCprList(a0),showlist
	rts	
.error
	move.l			#-1,d0	    
	rts
********************************************************************************************
*move gfx data from file buffer to display bitplanes.
movegfx
	clr.l	d0
	move.l	File_Buffer,a0
	move.l	work_bitmap,a1
	move.l	#depth-1,d2															planes counter
	move.l	#bm_Planes,d0													pick up offset to the Planes table
nextplane
	move.l	(a1,d0),a2																get next plane pointer				
	move.l	#height-1,d1
nextrow
	move.l	#width-1,d3						
nextbyte
	move.b	(a0)+,(a2)+
	dbra	d3,nextbyte
	dbra	d1,nextrow
	addq.l	#4,d0
	dbra d2,nextplane
	IFD TESTING_VBLANK
	rts
	ENDC
* do next bitmap
	clr.l	d0
	move.l	File_Buffer,a0
	move.l	show_bitmap,a1
	move.l	#depth-1,d2															planes counter
	move.l	#bm_Planes,d0													pick up offset to the Planes table
nextplane1
	move.l	(a1,d0),a2																get next plane pointer				
	move.l	#height-1,d1
nextrow1
	move.l	#width-1,d3						
nextbyte1
	move.b	(a0)+,(a2)+
	dbra	d3,nextbyte1
	dbra	d1,nextrow1
	addq.l	#4,d0
	dbra d2,nextplane1
	rts
******************************************************************************************
allocmem	
*get bitmap structure (Double buffering, so get two).    

 move.l			#sizex,d0																								PF width in pixels
 move.l			#sizey,d1																								PF height in lines
 move.l			#depth,d2																								Num bitplanes reqd
 move.l			#BMF_CLEAR|BMF_DISPLAYABLE,d3
 sub.l				a0,a0																												NULL pointer to friend bitmap
 movea.l			GrafBase,a6
 jsr						_LVOAllocBitMap(a6)
 tst.l				d0
	beq						.error_end
	move.l			d0,show_bitmap																			save pointer to alloc'd structure 

 move.l			#sizex,d0																								PF width in pixels
 move.l			#sizey,d1																								PF height in lines
 move.l			#depth,d2																								Num bitplanes reqd
 move.l			#BMF_CLEAR|BMF_DISPLAYABLE,d3
 sub.l				a0,a0																												NULL pointer to friend bitmap
 movea.l			GrafBase,a6
 jsr						_LVOAllocBitMap(a6)
 tst.l				d0
	beq						.error_end
	move.l			d0,work_bitmap																			save pointer to alloc'd structure 

*Get the memory I want for file buffer   
 move.l    #Filesize,d0         											file buffer.
 move.l    #MEMF_CLEAR|MEMF_CHIP,d1		     	any memory, cleared to zeros.
 movea.l   4.w,a6
 jsr       _LVOAllocMem(a6)
 tst.l     d0
 beq       .error_end																			couldn`t get memory.
 move.l    d0,File_Buffer																		save pointer to memory we got.

*Get memory for draw buffer. This is where we construct the objects.
	move.l    #width*height,d0         	  				memory size.
 move.l    #MEMF_CLEAR|MEMF_CHIP,d1		  				chip memory, cleared to zeros.
 movea.l   4.w,a6
 jsr       _LVOAllocMem(a6)
 tst.l     d0
 beq       .error_end 																	couldn`t get memory.
 move.l    d0,draw_buffer																save pointer to memory we got.
.end
	rts
.error_end
	move.l	#-1,d0
	rts

*-------------------------------------------------------------------------------------- 
FreeCop_Lists
	move.l	colormap,a0														freeup colormap structure memory
	movea.l	GrafBase,a6
	jsr					_LVOFreeColorMap(a6)
	
	lea					my_viewport,a0
	movea.l	GrafBase,a6
	jsr					_LVOFreeVPortCopLists(a6)	freeup intermediate lists

	move.l	showlist,a0	
	movea.l	GrafBase,a6
	jsr					_LVOFreeCprList(a6)					freeup hardware list
	
	move.l	worklist,a0	
	movea.l	GrafBase,a6
	jsr					_LVOFreeCprList(a6)					freeup hardware list

.done	

	rts
	
*--------------------------------------------------------------------------------------	

GetFile
* Entry d1 = Filename
*							d0 = Filebuffer      
	move.l       d0,-(a7)
 move.l       #MODE_OLDFILE,d2
 movea.l      DosBase,a6
 jsr          _LVOOpen(a6)
 move.l       d0,File_Handle									save filehandle
 move.l       (a7)+,d2
 tst.l        d0
 beq.s        File_Error
 move.l       d0,d1
 move.l       #Filesize,d3								
 movea.l      DosBase,a6
 jsr          _LVORead(a6)
 tst.l        d0
 bmi.s        File_Error
 move.l       d0,-(a7)
 move.l       File_Handle,d1									get filehandle
 movea.l      DosBase,a6
 jsr          _LVOClose(a6)
 move.l       (a7)+,d0
 rts

File_Error 
    moveq        #-1,d0
    rts
;--------------------------------------------------------------------------------------
; This routine adds the Lableadresses to all adresses in the objects.
; The face addresses and surfaces are stored in the load  file as offsets; this routine
; adds the base address to the offsets in order to create the useable addresses.

;Insert the Load-Lables after -objectsaddtable-!!!
	
objectsaddtable:
	dc.l	obj1coordstable
	dc.l	$ffffffff
	
objadd:
	lea	objectsaddtable(pc),a0
objadd1:
	move.l	(a0)+,d0
	cmp.l	#$ffffffff,d0
	beq.s	objaddend
	lea	1024,a2
	add.l	d0,a2
objadd2:
	cmp.l	#$ffffffff,(a2)+
	beq.s	objadd1
	add.l	d0,-4(a2)
	move.l	-4(a2),a3
objadd3:
	cmp.l	#$ffffffff,(a3)+
	beq.s	objadd2
	add.l	d0,-4(a3)	
	bra	objadd3
objaddend:
	rts
********************************************************************************************	
filledvectors:
	movem.l	d0-d7/a0-a6,-(a7)
* The workplanes are cleared of the old graphics data first...
clrscr
	lea	$dff000,a5

c_wait
	btst	#14,dmaconr(a5)
	bne.s	c_wait
	
* Set up blitter registers	
		move.l	workplanes,bltdpt(a5)
		move.w	#width/2,d0        								;x
		move.w	#depth*height,d1       				;y
		move.w	#0,bltdmod(a5)
		move.w	#%0000000100000000,bltcon0(a5)
		move.w	#0,bltcon1(a5)
		lsl.w		#6,d1
		or.w	d1,d0
		move.w	d0,bltsize(a5)
	
	bsr	vectorloop
	movem.l	(a7)+,d0-d7/a0-a6
	rts	
********************************************************************************************
vectorloop:
	movem.l	d0-d7/a0-a6,-(a7)
	lea	obj1table,a6
	lea	obj1pos,a0
	add.w	#0,12(a0)										alter x angle
	add.w	#0,14(a0)										alter y angle
	add.w	#38,16(a0)									alter z angle
	and.w	#$1fff,12(a0)						ensure size is within limits...
	and.w	#$1fff,14(a0)
	and.w	#$1fff,16(a0)
	bsr	coordscalc											rotate the object
	bsr	d2coordscalc									and calculate the coords reqd to draw it (2d ones)
	bsr	facetest													mark hidden faces
	bsr	drawobject											and draw the thing.
	movem.l	(a7)+,d0-d7/a0-a6
	rts
********************************************************************************************
*COORDSCALC: Transform the vertices to their new positions
coordscalc
	move.l	4(a6),a3
	move.l	12(a6),a4
	move.l	28(a6),d0
	move.l	(a4,d0),a4
	move.l	16(a6),a5	
coordscalcloop:
	move.w	(a4),d0									get next x coord
	cmp.w	#$7fff,d0								if its END OF TABLE quit
	beq	coordscalcloopend
	move.w	4(a4),d1								else pick up y
	move.w	8(a4),d2								and z
	
	sub.w	24(a3),d0
	sub.w	26(a3),d1
	sub.w	28(a3),d2
	move.w	12(a3),a0
	move.w	14(a3),a1
	move.w	16(a3),a2
	
	bsr	rotate
	
	add.w	24(a3),d0
	add.w	26(a3),d1
	add.w	28(a3),d2
	add.w	(a3),d0
	add.w	2(a3),d1
	add.w	4(a3),d2
	
	move.w	d0,(a5)+
	move.w	d1,(a5)+
	move.w	d2,(a5)+
	move.w	12(a4),(a5)+
	add.l	#14,a4									table is 3 longs plus one word = 14 bytes.
	bra	coordscalcloop
coordscalcloopend:
	move.w	#$7fff,(a5)
	rts
	
********************************************************************************************
* Once weve transformed the vertices we need to construct a table of coords for the blitter
* routines to use. 
d2coordscalc:
	move.l	16(a6),a4
	move.l	20(a6),a5
d2coordscalcloop:
	move.w	(a4)+,d0
	cmp.w	#$7fff,d0
	beq	d2coordscalcend
	move.w	(a4)+,d1
	move.w	(a4)+,d2
	ext.l	d0
	ext.l	d1
	ext.l	d2
	asl.l	#8,d0
	asl.l	#8,d1
	divs	d2,d0
	divs	d2,d1
	add.w	#yorigin,d1
	add.w	#xorigin,d0
	move.w	d0,(a5)
	move.w	d1,2(a5)
	move.w	(a4)+,4(a5)
	add.l	#8,a5
	bra	d2coordscalcloop
d2coordscalcend:
	move.w	#$7fff,(a5)
	rts
	
	***************
	*** facetest ***
	***************
	;   a4  ftable    a5  3dcoords
	
facetest:
	move.l	16(a6),a5
	move.l	12(a6),a4
	move.l	28(a6),d0
	move.l	4(a4,d0),a4
ftestloop:
	move.l	(a4)+,a0
	cmp.l	#$ffffffff,a0
	beq.s	ftestloopend
	move.l	(a0),a0
	move.l	a5,a1
	bsr	hiddenface
	bra	ftestloop
ftestloopend:
	rts
	
	
	*******************
	*** drawobject ***
	*******************	
	; a4  ftable    a5  2dcoords
drawobject:
	move.l	12(a6),a4
	move.l	28(a6),d0
	move.l	4(a4,d0),a4
	move.l	20(a6),a5	
drawloop:
	move.l	(a4)+,a2
	cmp.l	#$ffffffff,a2
	beq.s	drawloopend
drawloop2:
	move.l	(a2)+,a0
	cmp.l	#$ffffffff,a0
	beq.s	drawloop
	tst.w	(a0)
	beq.s	drawloop2
	move.l	a5,a1
	bsr	fill
	bra	drawloop2
drawloopend:
	rts
	
	*****************
	*** hiddenface ***
	*****************
	;a0   coordstable
	;a1   coords
	
hiddenface:
	movem.l	d0-d7/a0-a6,-(a7)
	move.w	6(a0),d7
	lsl.w	#3,d7
	move.w	(a1,d7.w),d0
	move.w	2(a1,d7.w),d1
	move.w	4(a1,d7.w),d2
	asr.w	#1,d0
	asr.w	#1,d1
	asr.w	#1,d2
	move.w	d0,a3
	move.w	d1,a4
	move.w	d2,a5
	move.w	2+6(a0),d7
	lsl.w	#3,d7
	move.w	(a1,d7.w),d0
	move.w	2(a1,d7.w),d1
	move.w	4(a1,d7.w),d2
	asr.w	#1,d0
	asr.w	#1,d1
	asr.w	#1,d2
	move.w	4+6(a0),d7
	lsl.w	#3,d7
	move.w	(a1,d7.w),d3
	move.w	2(a1,d7.w),d4
	move.w	4(a1,d7.w),d5
	asr.w	#1,d3
	asr.w	#1,d4
	asr.w	#1,d5
	sub.w	a3,d0	;v
	sub.w	a4,d1
	sub.w	a5,d2
	sub.w	a3,d3	;w
	sub.w	a4,d4
	sub.w	a5,d5
	move.w	d0,vx+2
	move.w	d0,vx2+2
	move.w	d1,vy+2
	move.w	d1,vy2+2
	move.w	d2,vz+2
	move.w	d2,vz2+2
	move.w	d3,d0
	move.w	d4,d1
	move.w	d5,d2
vy:
	muls	#0,d5
vz:
	muls	#0,d1
	sub.w	d1,d5
			;x
vz2:
	muls	#0,d3
vx:
	muls	#0,d2
	sub.w	d2,d3
			;y
vx2:
	muls	#0,d4
vy2:
	muls	#0,d0
	sub.w	d0,d4
			;z
	move.w	a3,d0
	move.w	a4,d1
	move.w	a5,d2
	
	muls	d0,d5
	muls	d1,d3
	muls	d2,d4
	add.l	d3,d4
	add.l	d4,d5
	tst.l	d5
	bge.s	unsich
sich:
	move.w	#0,(a0)
	bra.s	sichtend
unsich:
	move.w	#1,(a0)
sichtend:
	movem.l	(a7)+,d0-d7/a0-a6
	rts
	
	
	*************************
	**** Fill surfaces   ****
	*************************
	;a0 Tabelle mit Coordszeiger
	;a1 Coords
	
	
fill:
	movem.l	d0-d7/a2-a6,-(a7)
	lea	$dff000,a5
mc_wait
	btst	#14,dmaconr(a5)
	bne.s	mc_wait
	move.l	draw_buffer,bltdpt(a5)
	move.w	width/2,d0       											;x
	move.w	height,d1       											;y
	move.w	modu,bltdmod(a5)
	move.w	#%0000000100000000,bltcon0(a5)
	move.w	#0,bltcon1(a5)
	lsl.w	#6,d1
	or.w	d1,d0
	move.w	d0,bltsize(a5)

	move.w	2(a0),d7
	move.w	d7,coordsanzahl
	clr.w	fcolor
	move.b	5(a0),fcolor+1
	subq.w	#1,d7
	addq.l	#6,a0
	move.w	#$7fff,xminword
	move.w	#$7fff,yminword
	clr.w	xmaxword
	clr.w	ymaxword
	
	lea	fillcoordstable(pc),a5
makefillcoordstable:
	move.w	(a0)+,d0
	lsl.w	#3,d0
	move.l	a0,a3
	sub.l	a6,a3
	move.w	(a1,d0.w),d1
	move.w	2(a1,d0.w),d2
	cmp.w	xminword(pc),d1
	bge.s	xminset
	move.w	d1,xminword
xminset:
	cmp.w	yminword(pc),d2
	bge.s	yminset
	move.w	d2,yminword
yminset:
	cmp.w	xmaxword(pc),d1
	ble.s	xmaxset
	move.w	d1,xmaxword
xmaxset:
	cmp.w	ymaxword(pc),d2
	ble.s	ymaxset
	move.w	d2,ymaxword
ymaxset:
	move.w	d1,(a5)+
	move.w	d2,(a5)+
	dbf	d7,makefillcoordstable
	
	
	lea	fillcoordstable(pc),a2
	move.w	coordsanzahl(pc),d7
	subq.w	#2,d7
	move.w	xminword(pc),d4
	move.w	yminword(pc),a5
	IFND LINEDRAW 
	move.l	draw_buffer,a0
	ELSEIF
	move.l	workplanes,a0
	ENDC
	lea	40,a1
filldraw:
	move.w	(a2)+,d0
	move.w	(a2)+,d1
	move.w	(a2),d2
	move.w	2(a2),d3	
	sub.w	d4,d0
	sub.w	d4,d2
	sub.w	a5,d1
	sub.w	a5,d3
	cmp.w	d1,d3
	beq	fillend
	cmp.w	d1,d3
	bge.s	nochange
	exg	d1,d3
	exg	d0,d2
nochange:
	move.w	d2,d5
	move.w	d3,d6
	sub.w	d0,d5
	bpl.s	nomin
	neg.w	d5
nomin:
	sub.w	d1,d6
	asl.w	#1,d6
	cmp.w	d5,d6
	bgt.s	nomore
	subq.w	#1,d3
nomore:
	bsr	linedraw
fillend:
	dbf	d7,filldraw
	
	IFD LINEDRAW
	movem.l	(a7)+,d0-d7/a2-a6
	rts
	ENDC
		
	move.w	xmaxword(pc),d2
	move.w	ymaxword(pc),d3
	sub.w	d4,d2
	sub.w	a5,d3
	addq.w	#1,d3
	cmp.w	#2,d3
	blt	allfend
	moveq	#0,d1
	lsr.w	#4,d2
	addq.w	#1,d2
	move.w	d2,d1
	lsl.w	#1,d1
	moveq	#40,d7
	sub.w	d1,d7
	move.w	d2,with
	move.w	d3,high
	move.w	d7,modu
	lea	$dff000,a5
	
blittfillwait:
	btst	#14,2(a5)
	bne.s	blittfillwait
	move.l	#$ffffffff,$44(a5)
	move.w	d7,$64(a5)
	move.w	d7,bltdmod(a5)
	move.w	d3,d4
	mulu	#40,d4
	add.l	d1,d4
	sub.l	#82,d4
	add.l	draw_buffer,d4
	move.l	d4,$50(a5)
	move.l	d4,bltdpt(a5)
	move.w	#%0000100111110000,bltcon0(a5)
	move.w	#%1010,bltcon1(a5)
	subq.w	#1,d3
	lsl.w	#6,d3
	or.w	d3,d2
	move.w	d2,bltsize(a5)
fillwait:
	btst	#14,2(a5)
	bne.s	fillwait
	move.w	xminword(pc),d0
	move.w	yminword(pc),d1
	mulu	#40,d1
	move.w	d0,d2
	and.w	#$fff0,d0
	lsr.w	#3,d0
	add.w	d0,d1	
	
	add.l	workplanes,d1
	
	and.w	#$f,d2
	ror.w	#4,d2
	move.w	with(pc),d3
	move.w	high(pc),d4
	addq.w	#1,d3
	lsl.w	#6,d4
	or.w	d4,d3	
	move.w	modu,d0
	subq.w	#2,d0
	move.w	d0,$64(a5)
	clr.w	$62(a5)
	move.w	d0,bltdmod(a5)
	move.w	d0,$60(a5)
	move.w	fcolor(pc),d6	
	moveq	#3,d7
fcopyloop:
	move.l	d1,bltdpt(a5)
	move.l	d1,$48(a5)
	move.l	draw_buffer,$50(a5)
	btst	#0,d6
	beq.s	zeroplane	
	move.w	#%101111111010,d0	
	bra.s	zeroend
zeroplane:
	move.w	#%101100001010,d0	
zeroend:
	lsr.w	#1,d6
	or.w	d2,d0
	move.w	#0,bltcon1(a5)
	move.w	d0,bltcon0(a5)
	move.w	d3,bltsize(a5)
copywait:
	btst	#14,2(a5)
	bne.s	copywait
	add.l	#150*40,d1
	dbf	d7,fcopyloop	
allfend:	
	movem.l	(a7)+,d0-d7/a2-a6
	rts
;--------------------------	
fcolor:
	dc.w	0	
coordsanzahl:
	dc.w	0
high:
	dc.w	256
with:
	dc.w	20
modu:
	dc.w	0
xmaxword:
	dc.w	0
xminword:
	dc.w	$7fff
ymaxword:
	dc.w	0
yminword:
	dc.w	$7fff
fillcoordstable:
	dcb.w	400,0	
	********************
	*** 3-D Rotieren ***
	********************
	;d0,d1,d2   x,y,z
	;a0,a1,a2   umx,umy,umz
	
rotate:
	movem.l	d3-d7/a3-a6,-(a7)
	lea	costable(pc),a3
	lea	sintable,a4
	move.w	d0,d4
	move.w	d1,d5
	muls	(a3,a2.w),d4
	muls	(a4,a2.w),d5
	asr.l	#8,d4
	asr.l	#8,d5
	asr.l	#7,d4
	asr.l	#7,d5
	sub.w	d5,d4
	move.w	d4,d6
	move.w	d0,d4
	move.w	d1,d5
	muls	(a4,a2.w),d4
	muls	(a3,a2.w),d5
	asr.l	#8,d4
	asr.l	#8,d5
	asr.l	#7,d4
	asr.l	#7,d5
	add.w	d4,d5
	move.w	d5,d1
	move.w	d6,d0
rot_1:
	move.w	d2,d5
	move.w	d1,d4
	muls	(a3,a0.w),d4
	muls	(a4,a0.w),d5
	asr.l	#8,d4
	asr.l	#8,d5
	asr.l	#7,d4
	asr.l	#7,d5
	sub.w	d5,d4
	move.w	d4,d6
	move.w	d1,d4
	move.w	d2,d5
	muls	(a4,a0.w),d4
	muls	(a3,a0.w),d5
	asr.l	#8,d4
	asr.l	#8,d5
	asr.l	#7,d4
	asr.l	#7,d5
	add.w	d4,d5
	move.w	d5,d1
	move.w	d6,d2
rot_2:
	move.w	d0,d4
	move.w	d2,d5
	muls	(a3,a1.w),d4
	muls	(a4,a1.w),d5
	asr.l	#8,d4
	asr.l	#8,d5
	asr.l	#7,d4
	asr.l	#7,d5
	add.w	d5,d4
	move.w	d4,d6
	move.w	d0,d4
	move.w	d2,d5
	neg.w	d4
	muls	(a4,a1.w),d4
	muls	(a3,a1.w),d5
	asr.l	#8,d4
	asr.l	#8,d5
	asr.l	#7,d4
	asr.l	#7,d5
	add.w	d4,d5
	move.w	d5,d2
	move.w	d6,d0
rot_end:
	movem.l	(a7)+,d3-d7/a3-a6
	rts
	
	
	***********************
	**** Blitterlinien ****
	***********************
	;a1 Breite   a0 ADR
	
linedraw:
	movem.l	d4-d7/a2-a6,-(a7)
	cmp.w	d0,d2
	bne.s	li_weiter
	cmp.w	d1,d3
	beq	li_end
li_weiter:
	cmp.w	#0,d0
	blt	li_end
	cmp.w	#0,d1
	blt	li_end
	cmp.w	#0,d2
	blt	li_end
	cmp.w	#0,d3
	blt	li_end
	lea	$dff000,a5
	move.l	a1,d4
	mulu	d1,d4
	move.w	#$fff0,d5
	and.w	d0,d5
	lsr.w	#3,d5
	add.w	d5,d4
	add.l	a0,d4
	moveq	#0,d5
	sub.w	d1,d3
	roxl	d5
	tst.w	d3
	bge.s	li_y2gy1
	neg.w	d3
li_y2gy1:
	sub.w	d0,d2
	roxl	d5
	tst.w	d2
	bge.s	li_x2gx1
	neg.w	d2
li_x2gx1:
	move.w	d3,d1
	sub.w	d2,d1
	bge.s	li_dygdx
	exg	d2,d3
li_dygdx:
	roxl	d5
	move.b	li_okttable(pc,d5),d5
	add.w	d2,d2
li_wait:
	btst	#14,2(a5)
	bne.s	li_wait	
	move.w	d2,$62(a5)
	sub.w	d3,d2
	bge.s	li_signnl
	or.b	#$40,d5
li_signnl:
	
	move.w	d2,$52(a5)
	sub.w	d3,d2
	move.w	d2,$64(a5)
	move.w	#$8000,$74(a5)
	move.w	#$ffff,$72(a5)
	move.w	#$ffff,$44(a5)
	and.w	#$f,d0
	ror.w	#4,d0
	or.w	d0,d5
	or.w	#$0b4a,d0
	
	move.w	d0,bltcon0(a5)
	move.w	d5,bltcon1(a5)
	move.l	d4,$48(a5)
	move.l	d4,bltdpt(a5)
	move.l	d4,$4c(a5)
	move.w	a1,$60(a5)
	move.w	a1,bltdmod(a5)
	lsl.w	#6,d3
	addq.w	#2,d3
	move.w	d3,bltsize(a5)
	
li_end:			
	movem.l	(a7)+,d4-d7/a2-a6
	rts
li_okttable:
	dc.b	0*4+3
	dc.b	4*4+3
	dc.b	2*4+3
	dc.b	5*4+3
	dc.b	1*4+3
	dc.b	6*4+3
	dc.b	3*4+3
	dc.b	7*4+3
li_point:
	move.w	a1,d5
	mulu	d5,d1
	and.l	#$fff8,d0
	lsr.w	#3,d0
	add.l	d0,d1
	add.l	d1,a0
	and.l	#$7,d2
	moveq	#7,d5
	sub.l	d2,d5
	btst	d5,(a0)
	beq.s	setit3
	bclr	d5,(a0)
	bra.s	esetend
setit3:
	bset	d5,(a0)
esetend:
	bra	li_end
************************************************************************************
vblank_code
	movem.l	d0/a6,-(sp)
* switch display memory pointers over	
	move.l		showplanes,d0
	move.l		workplanes,showplanes
	move.l		d0,workplanes

* switch copperlists	
	move.l		showlist,d0
	move.l		worklist,showlist
	move.l		d0,worklist
	
	IFND USE_VBLANK
	movea.l	GrafBase,a6
	jsr	_LVOWaitTOF(a6)
	ENDC

* load copperlist pointer
	lea					my_view,a1
	move.l		showlist,v_LOFCprList(a1)
	
	movea.l	GrafBase,a6
	lea					my_view,a1
	jsr					_LVOLoadView(a6)
		
;	clr.l	vbi_flag
.vblank_done	
	movem.l	(sp)+,d0/a6
	IFD USE_VBLANK
	moveq.l	#0,d0													I have to do this for the OS
	ENDC
	rts
************************************************************************************
* Variables
gfxversion					ds.w	1																											;lib version
pad												ds.w	1																											;make following ptrs LWord aligned
show_bitmap				ds.l	1
work_bitmap				ds.l	1
showlist							ds.l	1
worklist							ds.l	1
showplanes					ds.l	1
workplanes					ds.l	1

vextra									ds.l	1
modeid									ds.l	1
monspec								ds.l	1
vpextra								ds.l	1							
File_Buffer				ds.l	1
DosBase								ds.l	1
GrafBase							ds.l	1
IntuiBase						ds.l	1
LowLevelBase			ds.l	1
OldActiView				ds.l	1
File_Handle				ds.l	1
intHandle						ds.l	1
Old_SPRITERESN	ds.l	1							
Screen_Ptr					ds.l	1
colormap							ds.l	1
ReturnMsg						ds.l	1
draw_buffer				ds.l	1
bitplanes						ds.l	depth

*STRING CONSTANTS
LowLevelName		dc.b	'lowlevel.library',0
	EVEN
WBench_Name		dc.b	'Workbench',0
	EVEN
GrafName					dc.b	'graphics.library',0
	EVEN
IntuiName				dc.b	'intuition.library',0
	EVEN
DosName						dc.b	'dos.library',0
	EVEN
File									dc.b	'DATA:Polyback.bin',0
	EVEN
color_table
	dc.w	$0000	;color 0
	dc.w	$0555
	dc.w	$0666
	dc.w	$0666
	dc.w	$0777
	dc.w	$0888
	dc.w	$0999
	dc.w	$0999
	dc.w	$0fdf
	dc.w	$0ebf
	dc.w	$0d9f
	dc.w	$0c7f
	dc.w	$0c5f
	dc.w	$0b3f
	dc.w	$0a2f
	dc.w	$0a0f	;color 15

	EVEN	
areabuffer   dcb.b	NUM_VERTICES*5,0

* SYSTEM STRUCTURES I WANT TO USE.
	EVEN
vblank	ds.b	IS_SIZE	;this is where the interrupt structure is stored.
	EVEN 
my_view						ds.b	v_SIZEOF
	EVEN
my_viewport		ds.b	vp_SIZEOF
	EVEN
my_rasinfo			ds.b	ri_SIZEOF
	EVEN
dimquery					ds.b	dis_SIZEOF
	EVEN
rastport					ds.b	rp_SIZEOF
	EVEN
areainfo					ds.b	ai_SIZEOF
		EVEN
tmpras							ds.b	tr_SIZEOF
*******************************************************************************************
;Obj1	
obj1table:
	dc.w	0,0
	dc.l	obj1pos,0,obj1coords,obj1table1,obj1table2
	dc.l	0,0,0,0
	
obj1coords:
	dc.l	obj1coordstable,obj1coordstable+1024
	dc.l	$ffffffff
	
obj1pos:
	dc.w	0,0,600
	dc.w	0,0,0
	dc.w	0,0,0
	dc.w	0,0,0
	dc.w	0,0,0
	dc.w	0,0,0
		
obj1coordstable:
 	INCBIN   myasmcode:rework/polygons/oldstuff/.F
	even
obj1table1:
	dcb.b	1000,0
	even
obj1table2:
	dcb.b	1000,0
	even	
***************************************************************************************	
flaechensorttable:
	dcb.b	1024*21,0
	even
***************************************************************************************
costable:	
	include Table_of_Cosines	
		
sintable:
	include Table_of_Sines
	
file_end					dc.b	'End of File'

	