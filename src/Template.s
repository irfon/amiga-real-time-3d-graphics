*****************************************************************************************
*                                                               * 
*                                                     *
*****************************************************************************************
USE_FILES        EQU 1
DOUBLE_BUFFERING EQU 1
;MY_NTSC          EQU 1  If set HEIGHT = 200 else = 256.
;USE_VBLANK       EQU 1
;TESTING_VBLANK   EQU 1		switch to turn only writing loaded gfx to one bitmap
;THIRTYTWOCOLOURS EQU 1
SIXTEENCOLOURS   EQU 1
;KILLTASKS        EQU 1
****************************************************************************************	
	include startup.s
 
 include core_01.s
 include core_02.s
 include data_01.s
 include data_02.s
 include bss_02.s
**************************************************************************************** 
display
	movem.l	d0-d7/a0-a6,-(sp)
	IFD KILLTASKS
	movea.l	$4.w,a6
		jsr	_LVOForbid(a6)           This wont work if you use WaitTOF().		
	ENDC
displayloop	
	IFND USE_VBLANK
	jsr	vblank_code
	ENDC
*****************************************************************************************
* The next few lines should be retained for all programs as they clear the workplanes of
* old data, this routine is called at the start of drw1_shw2 & drw2_shw1 in the book.
* (DON'T look for these I haven't used them!).
	
	IFD DOUBLE_BUFFERING	
 move.l workplanes,a0        pointer to bitplane lists
 ELSEIF
 move.l showplanes,a0        pointer to alternative lists
 ENDC
 move.l #HEIGHT,d0    
 lsl.w  #SIXTYFOUR,d0									shift HEIGHT into correct register bits
 or.w   #WIDTH/2,d0											add width
 move.l #$dff000,a5
 bsr    clear_blit
***************************************************************************************** 
	
	btst			#6,$bfe001												are we fed up yet?
	bne 			displayloop																		no, not yet....
all_over	
	IFD KILLTASKS			
	movea.l	$4.w,a6
		jsr	_LVOPermit(a6)
	ENDC
	movem.l (sp)+,d0-d7/a0-a6	
	rts
**********************************************************************************	
